# <%=$PLASTER_PARAM_PackageName%>

<%=$PLASTER_PARAM_PackageDesc%>

## Next steps

1. Place all publicly-exposed functions into `<%=$PLASTER_PARAM_PackageName%>\Public`
2. Place all private functions into `<%=$PLASTER_PARAM_PackageName%>\Private`