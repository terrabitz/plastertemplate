﻿Set-StrictMode -Version Latest

#Define global module variables:
New-Variable -Name ModuleRoot -Value $PSScriptRoot -Option ReadOnly
<%
if ($PLASTER_PARAM_ModuleFolderOptions -contains 'etc') {
    "New-Variable -Name etcPath -Value (Join-Path -Path `$PSScriptRoot -ChildPath 'etc') -Option ReadOnly"
}
if ($PLASTER_PARAM_ModuleFolderOptions -contains 'lib') {
    "New-Variable -Name libPath -Value (Join-Path -Path `$PSScriptRoot -ChildPath 'lib') -Option ReadOnly"
}
if ($PLASTER_PARAM_ModuleFolderOptions -contains 'bin') {
    "New-Variable -Name binPath -Value (Join-Path -Path `$PSScriptRoot -ChildPath 'bin') -Option ReadOnly"
}
%>

# Get public and private function definition files:
$Classes = @(Get-ChildItem -Path $ModuleRoot\Classes  -Include *.ps1 -Recurse -ErrorAction SilentlyContinue)
$Public = @(Get-ChildItem -Path $ModuleRoot\Public   -Include *.ps1 -Recurse -ErrorAction SilentlyContinue)
$Private = @(Get-ChildItem -Path $ModuleRoot\Private  -Include *.ps1 -Recurse -ErrorAction SilentlyContinue)

# Dot source the files:
foreach ($Import in @($Classes + $Public + $Private)) {
    try {
        . $Import.FullName
    } catch {
        Write-Error -Message "Failed to import function $($Import.BaseName): $_"
    }
}
Export-ModuleMember -Function $Public.BaseName