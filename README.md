# PlasterTemplate

This is my personal setup for new PowerShell projects.

## Getting started

1. Pull down this project into a local folder

    ```powershell
    cd \Path\to\my\projects
    git clone git@gitlab.com:terrabitz/plastertemplate.git
    ```

2. Install `Plaster`:

    ```powershell
    Install-Module Plaster -Scope CurrentUser
    ```

3. Use this template to initialize the new project:

    ```powershell
    Invoke-Plaster -TemplatePath .\PlasterTemplate -DestinationPath .\MyNewModule
    ```

4. Fill in the prompts to create the package

5. Alternatively, uncomment and use any components in the  `answers.psd1` file:

    ```powershell
    cp ./answers.psd1 ~/my_answers.psd1
    $answers = Import-PowerShellDataFile ~/my_answers.psd1
    Invoke-Plaster @answers -TemplatePath ./PlasterTemplate -DestinationPath ./MyNewModule
    ```